��          �   %   �      p  
   q     |     �  3   �     �  ^   �  d   T  8   �  !   �  E     Z   Z     �     �  .   �  V   �  �   U  �   )     �     �  @   �  (   $     M  (   e  r   �  	               �  )  
   �	     �	     �	  3   �	     
  ^   2
  d   �
  8   �
  !   /  E   Q  Z   �     �     �  .     V   ;  �   �  �   f     �       @      (   a     �  (   �  r   �  	   >     H     W           
                                                                                            	                    .extension Activer le renommage Activer les redirections Actuellement, vos fichiers seront renommés ainsi : Automatic File Renamer Ce nombre varie. il permet de différencier deux enregistrements avec le même nom de fichier. Ce réglage s'appliquera à tous les fichiers qui seront téléversés.<br>Ce n'est pas rétroactif. Cela peut être très embêtant, pour pleins de raisons  Choisissez des termes évocateurs Choisissez quel(s) role(s) peuvent modifier les réglages ci-dessus : D'autant plus qu'une fois que la page est créée, il est très dangereux de la renommer ! Documention Enregistré ! La redirection permet de cacher la page liée. Les moteurs de recherche (et vous aussi) aiment quand les mots décrivent le contenu ! Lors du téléversement d'un média, <b>une page liée est automatiquement créée</b>.<br>
  <p>Si votre fichier a pour nom "<b>cheval.jpg</b>", la page s'appelera "<b>https://votresite.fr/cheval</b>".
    <br> Pour éviter cela, il est possible grâce à ce plugin de renommer automatiquement vos fichiers et leur pages liées lors de leur téléversement. Redirection activée Renommage activé Renvoie sur la page attachée, si elle existe (sinon erreur 404) Renvoie sur le fichier média (défaut). Renvoie une erreur 404. Réglages réservés aux administrateurs Une fois enregistré, le nom du fichier n'est plus modifiable. Choisissez le nom AVANT de téléverser le fichier. extension nom-du-fichier évoquées ici Project-Id-Version: Automatic-File-Renamer
PO-Revision-Date: 2022-02-24 10:56+0100
Last-Translator: 
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 3.0.1
X-Poedit-Basepath: ../Admin
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: AFR-admin-Panel.php
 .extension Activer le renommage Activer les redirections Actuellement, vos fichiers seront renommés ainsi : Automatic File Renamer Ce nombre varie. il permet de différencier deux enregistrements avec le même nom de fichier. Ce réglage s'appliquera à tous les fichiers qui seront téléversés.<br>Ce n'est pas rétroactif. Cela peut être très embêtant, pour pleins de raisons  Choisissez des termes évocateurs Choisissez quel(s) role(s) peuvent modifier les réglages ci-dessus : D'autant plus qu'une fois que la page est créée, il est très dangereux de la renommer ! Documention Enregistré ! La redirection permet de cacher la page liée. Les moteurs de recherche (et vous aussi) aiment quand les mots décrivent le contenu ! Lors du téléversement d'un média, <b>une page liée est automatiquement créée</b>.<br>
  <p>Si votre fichier a pour nom "<b>cheval.jpg</b>", la page s'appelera "<b>https://votresite.fr/cheval</b>".
    <br> Pour éviter cela, il est possible grâce à ce plugin de renommer automatiquement vos fichiers et leur pages liées lors de leur téléversement. Redirection activée Renommage activé Renvoie sur la page attachée, si elle existe (sinon erreur 404) Renvoie sur le fichier média (défaut). Renvoie une erreur 404. Réglages réservés aux administrateurs Une fois enregistré, le nom du fichier n'est plus modifiable. Choisissez le nom AVANT de téléverser le fichier. extension nom-du-fichier évoquées ici 
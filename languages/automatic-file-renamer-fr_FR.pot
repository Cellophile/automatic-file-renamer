#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Automatic-File-Renamer\n"
"POT-Creation-Date: 2022-02-24 10:55+0100\n"
"PO-Revision-Date: 2022-02-05 12:33+0100\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Poedit 3.0.1\n"
"X-Poedit-Basepath: ../Admin\n"
"X-Poedit-KeywordsList: __;_e\n"
"X-Poedit-SearchPath-0: AFR-admin-Panel.php\n"

#: AFR-admin-Panel.php:109
msgid "Automatic File Renamer"
msgstr ""

#: AFR-admin-Panel.php:111
msgid ""
"Lors du téléversement d'un média, <b>une page liée est automatiquement "
"créée</b>.<br>\n"
"  <p>Si votre fichier a pour nom \"<b>cheval.jpg</b>\", la page s'appelera "
"\"<b>https://votresite.fr/cheval</b>\".\n"
"    <br>"
msgstr ""

#: AFR-admin-Panel.php:114
msgid "Cela peut être très embêtant, pour pleins de raisons "
msgstr ""

#: AFR-admin-Panel.php:114
msgid "évoquées ici"
msgstr ""

#: AFR-admin-Panel.php:115
msgid ""
"D'autant plus qu'une fois que la page est créée, il est très dangereux de la "
"renommer !"
msgstr ""

#: AFR-admin-Panel.php:116
msgid ""
"Pour éviter cela, il est possible grâce à ce plugin de renommer "
"automatiquement vos fichiers et leur pages liées lors de leur téléversement."
msgstr ""

#: AFR-admin-Panel.php:118
msgid "Choisissez des termes évocateurs"
msgstr ""

#: AFR-admin-Panel.php:119
msgid ""
"Les moteurs de recherche (et vous aussi) aiment quand les mots décrivent le "
"contenu !"
msgstr ""

#: AFR-admin-Panel.php:128 AFR-admin-Panel.php:137
msgid "nom-du-fichier"
msgstr ""

#: AFR-admin-Panel.php:130
msgid ".extension"
msgstr ""

#: AFR-admin-Panel.php:133 AFR-admin-Panel.php:202
msgid "Enregistré !"
msgstr ""

#: AFR-admin-Panel.php:136
msgid "Actuellement, vos fichiers seront renommés ainsi :"
msgstr ""

#: AFR-admin-Panel.php:137
msgid ""
"Une fois enregistré, le nom du fichier n'est plus modifiable. Choisissez le "
"nom AVANT de téléverser le fichier."
msgstr ""

#: AFR-admin-Panel.php:137
msgid ""
"Ce nombre varie. il permet de différencier deux enregistrements avec le même "
"nom de fichier."
msgstr ""

#: AFR-admin-Panel.php:137
msgid "extension"
msgstr ""

#: AFR-admin-Panel.php:139
msgid ""
"Ce réglage s'appliquera à tous les fichiers qui seront téléversés.<br>Ce "
"n'est pas rétroactif."
msgstr ""

#: AFR-admin-Panel.php:143
msgid "La redirection permet de cacher la page liée."
msgstr ""

#: AFR-admin-Panel.php:151
msgid "Renvoie sur le fichier média (défaut)."
msgstr ""

#: AFR-admin-Panel.php:155
msgid "Renvoie sur la page attachée, si elle existe (sinon erreur 404)"
msgstr ""

#: AFR-admin-Panel.php:159
msgid "Renvoie une erreur 404."
msgstr ""

#: AFR-admin-Panel.php:173
msgid "Réglages réservés aux administrateurs"
msgstr ""

#: AFR-admin-Panel.php:174
msgid "Choisissez quel(s) role(s) peuvent modifier les réglages ci-dessus :"
msgstr ""

#: AFR-admin-Panel.php:211
msgid "Documention"
msgstr ""

#: AFR-admin-Panel.php:239
msgid "Renommage activé"
msgstr ""

#: AFR-admin-Panel.php:243
msgid "Activer le renommage"
msgstr ""

#: AFR-admin-Panel.php:266
msgid "Redirection activée"
msgstr ""

#: AFR-admin-Panel.php:271
msgid "Activer les redirections"
msgstr ""
